package model

import (
	"encoding/json"
	"fmt"
	"io"
	"regexp"
	"strings"

	"gopkg.in/mgo.v2/bson"
)

type Project struct {
	ID        bson.ObjectId `json:"-" bson:"_id"`
	Name      string        `json:"name"`
	StartDate *Time         `json:"start_date,omitempty"`
	EndDate   *Time         `json:"end_date,omitempty"`
	Slug      string        `json:"slug"`
	Tasks     []Task        `json:"tasks"`
}

func (p *Project) PreSave() {
	// assigning new id
	p.ID = bson.NewObjectId()

	// creating slug
	slug := p.Name
	slug = strings.ToLower(slug)

	// removing whitespaces with dash
	re := regexp.MustCompile(`\s+`)
	slug = re.ReplaceAllString(slug, "-")

	//removing special characters
	re = regexp.MustCompile(`[^\w-]`)
	slug = re.ReplaceAllString(slug, "")

	//trimming dashes
	slug = strings.Trim(slug, "-")

	p.Slug = slug
}

func ProjectFromJSON(data io.Reader) *Project {
	decoder := json.NewDecoder(data)
	var project Project

	err := decoder.Decode(&project)
	if err != nil {
		return nil
	}

	return &project
}

func ProjectMapToJson(projectMap map[string]Project) string {
	body, err := json.Marshal(projectMap)
	if err != nil {
		return ""
	} else {
		return string(body)
	}
}

func (p *Project) ToJson() string {
	b, err := json.Marshal(p)
	if err != nil {
		return ""
	} else {
		return string(b)
	}
}

func (p *Project) Validate() error {
	if len(p.Name) == 0 {
		return fmt.Errorf("invalid name")
	}

	return nil
}
