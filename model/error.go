package model

import (
	"encoding/json"
	"net/http"
)

func ErrorString(errMsg string, w http.ResponseWriter) {
	errorMap := map[string]string{"error": errMsg}
	w.WriteHeader(500)
	b, err := json.Marshal(errorMap)
	if err != nil {
		w.Write([]byte(""))
	} else {
		w.Write(b)
	}
}
