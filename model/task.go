package model

import (
	"encoding/json"
	"fmt"
	"io"

	"gopkg.in/mgo.v2/bson"
)

type Task struct {
	ID          bson.ObjectId `json:"-" bson:"_id"`
	Title       string        `json:"title"`
	Description string        `json:"description"`
	Status      string        `json:"status"`
}

func (t *Task) PreSave() {
	t.ID = bson.NewObjectId()
	t.Status = "open"
}

func TaskFromJSON(data io.Reader) *Task {
	decoder := json.NewDecoder(data)
	var task Task

	err := decoder.Decode(&task)
	if err != nil {
		return nil
	}

	return &task
}

func (t *Task) ToJson() string {
	b, err := json.Marshal(t)
	if err != nil {
		return ""
	} else {
		return string(b)
	}
}

func TasksToJson(tasks []Task) string {
	b, err := json.Marshal(tasks)
	if err != nil {
		return ""
	}

	return string(b)
}

func (t *Task) Validate() error {
	if len(t.Title) == 0 {
		return fmt.Errorf("invalid title")
	}

	return nil
}
