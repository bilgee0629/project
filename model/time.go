package model

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

type Time struct {
	time.Time
}

func (t *Time) UnmarshalJSON(body []byte) error {
	bodyString := strings.Trim(string(body), `"`)
	s := strings.Split(bodyString, `-`)
	if len(s) != 3 {
		return fmt.Errorf("Error parsing date: invalid date")
	}

	year, err := strconv.Atoi(s[0])
	if err != nil {
		return fmt.Errorf("Error parsing date: %v", err)
	}

	month, err := strconv.Atoi(s[1])
	if err != nil {
		return fmt.Errorf("Error parsing date: %v", err)
	} else if month > 12 || month < 1 {
		return fmt.Errorf("Error parsing date: wrong month")
	}

	day, err := strconv.Atoi(s[2])
	if err != nil {
		return fmt.Errorf("Error parsing date: %v", err)
	}

	date := time.Date(year, time.Month(month), day, 00, 00, 00, 00, time.UTC)
	*t = Time{date}

	return nil
}
