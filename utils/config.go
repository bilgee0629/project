package utils

import "github.com/spf13/viper"

func LoadConfig(configPaths []string, configName string) {
	viper.SetConfigName(configName)
	for _, configPath := range configPaths {
		viper.AddConfigPath(configPath)
	}
	err := viper.ReadInConfig()
	if err != nil {
		panic("cannot load config file: " + err.Error())
	}
}
