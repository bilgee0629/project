package api

import (
	"bitbucket.org/bilgee0629/project/app"
	"github.com/gorilla/mux"
)

type Routes struct {
	Root     *mux.Router
	Tasks    *mux.Router
	Projects *mux.Router
}

var BaseRoutes *Routes

func InitAPI() {
	BaseRoutes = &Routes{}
	BaseRoutes.Root = app.Srv.Router.PathPrefix("/api/v1").Subrouter()
	BaseRoutes.Tasks = BaseRoutes.Root.PathPrefix("/tasks").Subrouter()
	BaseRoutes.Projects = BaseRoutes.Root.PathPrefix("/projects").Subrouter()

	InitProjects()
	InitTasks()
}
