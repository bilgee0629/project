package api

import (
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/bilgee0629/project/app"
	"bitbucket.org/bilgee0629/project/model"

	"github.com/sirupsen/logrus"
)

func InitProjects() {
	logrus.Info("initializing projects")
	BaseRoutes.Projects.HandleFunc("", CreateProject).Methods("POST")
	BaseRoutes.Projects.HandleFunc("", GetProjects).Methods("GET")
}

func CreateProject(w http.ResponseWriter, r *http.Request) {
	project := model.ProjectFromJSON(r.Body)
	resultProject, err := app.CreateProject(project)
	if err != nil {
		model.ErrorString(err.Error(), w)
		return
	}

	w.Write([]byte(resultProject.ToJson()))
}

func GetProjects(w http.ResponseWriter, r *http.Request) {
	name := r.URL.Query().Get("name")
	limit := r.URL.Query().Get("limit")
	offset := r.URL.Query().Get("offset")

	var projects []model.Project
	var err error

	// when user requests filtered projects
	if len(name) > 0 || len(limit) > 0 {

		if len(limit) > 0 {
			limitInt := 0
			offsetInt := 0

			limitInt, err = strconv.Atoi(limit)
			if err != nil {
				model.ErrorString(fmt.Sprintf("invalid limit parameter: %v", err), w)
				return
			}

			if len(offset) > 0 {
				offsetInt, err = strconv.Atoi(offset)
				if err != nil {
					model.ErrorString(fmt.Sprintf("invalid offset parameter: %v", err), w)
					return
				}
			}

			projects, err = app.GetFilteredProjects(name, limitInt, offsetInt)
			if err != nil {
				model.ErrorString(fmt.Sprintf("can't get projects: %v", err), w)
				return
			}

		} else {
			projects, err = app.GetFilteredProjects(name, 0, 0)
			if err != nil {
				model.ErrorString(fmt.Sprintf("can't get projects: %v", err), w)
				return
			}
		}

	} else {
		projects, err = app.GetProjects()
		if err != nil {
			model.ErrorString(err.Error(), w)
			return
		}
	}

	projectMap := make(map[string]model.Project)

	for _, v := range projects {
		projectMap[v.ID.Hex()] = v
	}

	w.Write([]byte(model.ProjectMapToJson(projectMap)))
}
