package api

import (
	"net/http"

	"bitbucket.org/bilgee0629/project/app"
	"bitbucket.org/bilgee0629/project/model"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

func InitTasks() {
	logrus.Info("initializing tasks")
	BaseRoutes.Projects.HandleFunc("/{id}/tasks", CreateTask).Methods("POST")
	BaseRoutes.Projects.HandleFunc("/{id}/tasks", GetTasks).Methods("GET")
	BaseRoutes.Projects.HandleFunc("/{id}/tasks/{task_id}/close", CloseTask).Methods("GET")
}

func GetTasks(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectID := vars["id"]

	status := r.URL.Query().Get("status")
	var tasks []model.Task
	var err error

	if len(status) > 0 {
		tasks, err = app.GetFilteredTasks(projectID, status)
	} else {
		tasks, err = app.GetTasks(projectID)
	}

	if err != nil {
		model.ErrorString(err.Error(), w)
		return
	}

	w.Write([]byte(model.TasksToJson(tasks)))
}

func CreateTask(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectID := vars["id"]

	task := model.TaskFromJSON(r.Body)

	resultTask, err := app.CreateTask(projectID, task)
	if err != nil {
		model.ErrorString(err.Error(), w)
		return
	}

	w.Write([]byte(resultTask.ToJson()))
}

func CloseTask(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectID := vars["id"]
	taskID := vars["task_id"]

	task, err := app.CloseTask(projectID, taskID)
	if err != nil {
		model.ErrorString(err.Error(), w)
		return
	}

	w.Write([]byte(task.ToJson()))
}
