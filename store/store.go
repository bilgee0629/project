package store

import "bitbucket.org/bilgee0629/project/model"

type StoreResult struct {
	Data interface{}
	Err  error
}

type StoreChannel chan StoreResult

type Store interface {
	Project() ProjectStore
	Task() TaskStore
}

type ProjectStore interface {
	Save(project *model.Project) StoreChannel
	Get(id string) StoreChannel
	GetAll() StoreChannel
	GetFilteredProjects(name string, limit int, offset int) StoreChannel
}

type TaskStore interface {
	Save(projectID string, task *model.Task) StoreChannel
	Get(projectID string, taskID string) StoreChannel
	GetAll(projectID string) StoreChannel
	CloseTask(projectID string, taskID string) StoreChannel
}
