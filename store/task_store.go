package store

import (
	"bitbucket.org/bilgee0629/project/model"
	"gopkg.in/mgo.v2/bson"
)

type MongoTaskStore struct {
	MongoStore
}

func NewMongoTaskStore(mongoStore MongoStore) TaskStore {
	return &MongoTaskStore{mongoStore}
}

func (m *MongoTaskStore) Save(projectID string, task *model.Task) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		task.PreSave()
		if err := m.GetDB().C("projects").Update(bson.M{"_id": bson.ObjectIdHex(projectID)}, bson.M{"$push": bson.M{"tasks": task}}); err != nil {
			result.Err = err
		} else {
			result.Data = task
		}
		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}

func (m *MongoTaskStore) Get(projectID, taskID string) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		project := model.Project{}
		if err := m.GetDB().C("projects").Find(bson.M{"_id": bson.ObjectIdHex(projectID)}).Select(bson.M{"tasks": bson.M{"$elemMatch": bson.M{"_id": bson.ObjectIdHex(taskID)}}}).One(&project); err != nil {
			result.Err = err
		} else {
			result.Data = project.Tasks[0]
		}
		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}

func (m *MongoTaskStore) GetAll(projectID string) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		project := model.Project{}

		if err := m.GetDB().C("projects").Find(bson.M{"_id": bson.ObjectIdHex(projectID)}).Select(bson.M{"tasks": 1}).One(&project); err != nil {
			result.Err = err
		} else {
			result.Data = project.Tasks
		}
		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}

func (m *MongoTaskStore) CloseTask(projectID string, taskID string) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}

		if err := m.GetDB().C("projects").Update(bson.M{"_id": bson.ObjectIdHex(projectID), "tasks._id": bson.ObjectIdHex(taskID)}, bson.M{"$set": bson.M{"tasks.$.status": "closed"}}); err != nil {
			result.Err = err
		}
		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}
