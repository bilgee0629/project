package store

import (
	"fmt"

	"bitbucket.org/bilgee0629/project/model"
	"gopkg.in/mgo.v2/bson"
)

type MongoProjectStore struct {
	MongoStore
}

func NewMongoProjectStore(mongoStore MongoStore) ProjectStore {
	return &MongoProjectStore{mongoStore}
}

func (m *MongoProjectStore) Get(id string) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		project := model.Project{}
		if err := m.GetDB().C("projects").FindId(id).One(&project); err != nil {
			result.Err = err
		} else {
			result.Data = project
		}

		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}

func (m *MongoProjectStore) GetAll() StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		projects := []model.Project{}
		if err := m.GetDB().C("projects").Find(bson.M{}).All(&projects); err != nil {
			result.Err = err
		} else {
			result.Data = projects
		}

		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}

func (m *MongoProjectStore) Save(project *model.Project) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}
		project.PreSave()

		if err := m.GetDB().C("projects").Insert(project); err != nil {
			result.Err = err
		} else {
			result.Data = project
		}

		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}

func (m *MongoProjectStore) GetFilteredProjects(name string, limit int, offset int) StoreChannel {
	storeChannel := make(StoreChannel, 1)
	go func() {
		result := StoreResult{}

		var selector bson.M
		if len(name) > 0 {
			selector = bson.M{"name": bson.RegEx{fmt.Sprintf("%s", name), ""}}
		} else {
			selector = nil
		}

		query := m.GetDB().C("projects").Find(selector)

		if limit > 0 {
			query = query.Limit(limit)
		}

		if offset > 0 && limit > 0 {
			query = query.Skip(limit * offset)
		}

		var projects []model.Project

		if err := query.All(&projects); err != nil {
			result.Err = err
		} else {
			result.Data = projects
		}

		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}
