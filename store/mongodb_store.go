package store

import (
	"fmt"

	mgo "gopkg.in/mgo.v2"
)

type MongoStore struct {
	project ProjectStore
	task    TaskStore
	db      *mgo.Database
}

func NewMongoStore() Store {
	mongoStore := MongoStore{}

	mongoStore.db = setupConnection()
	mongoStore.project = NewMongoProjectStore(mongoStore)
	mongoStore.task = NewMongoTaskStore(mongoStore)

	return mongoStore
}

func setupConnection() *mgo.Database {
	session, err := mgo.Dial("mongodb://localhost")
	if err != nil {
		panic(fmt.Sprintf("cannot connect to mongodb: %v", err))
	}

	return session.DB("project")
}

func (m MongoStore) Project() ProjectStore {
	return m.project
}

func (m MongoStore) Task() TaskStore {
	return m.task
}

func (m MongoStore) GetDB() *mgo.Database {
	return m.db
}
