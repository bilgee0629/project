package app

import (
	"fmt"

	"bitbucket.org/bilgee0629/project/model"
)

func CreateTask(projectID string, task *model.Task) (*model.Task, error) {
	if task == nil {
		return nil, fmt.Errorf("invalid payload")
	}
	if len(projectID) == 0 {
		return nil, fmt.Errorf("invalid project id")
	}
	if err := task.Validate(); err != nil {
		return nil, err
	}
	if result := <-Srv.Store.Project().Get(projectID); result.Err != nil {
		return nil, fmt.Errorf("project not found")
	}
	if result := <-Srv.Store.Task().Save(projectID, task); result.Err != nil {
		return nil, result.Err
	} else {
		resultProject := result.Data.(*model.Task)
		return resultProject, nil
	}
}

func GetTasks(projectID string) ([]model.Task, error) {
	if len(projectID) == 0 {
		return nil, fmt.Errorf("invalid project id")
	}
	if result := <-Srv.Store.Project().Get(projectID); result.Err != nil {
		return nil, fmt.Errorf("project not found")
	}
	if result := <-Srv.Store.Task().GetAll(projectID); result.Err != nil {
		return nil, result.Err
	} else {
		resultProject := result.Data.(model.Project)
		return resultProject.Tasks, nil
	}
}

func GetFilteredTasks(projectID string, status string) ([]model.Task, error) {
	if result := <-Srv.Store.Project().Get(projectID); result.Err != nil {
		return nil, fmt.Errorf("project not found")
	}
	if result := <-Srv.Store.Task().GetAll(projectID); result.Err != nil {
		return nil, result.Err
	} else {
		resultProjects := result.Data.([]model.Task)
		tasks := []model.Task{}
		for _, task := range resultProjects {
			if task.Status == status {
				tasks = append(tasks, task)
			}
		}
		return tasks, nil
	}
}

func CloseTask(projectID string, taskID string) (*model.Task, error) {
	if len(projectID) == 0 {
		return nil, fmt.Errorf("invalid project id")
	}
	if len(taskID) == 0 {
		return nil, fmt.Errorf("invalid task id")
	}
	if result := <-Srv.Store.Project().Get(projectID); result.Err != nil {
		return nil, fmt.Errorf("project not found")
	}
	if result := <-Srv.Store.Task().CloseTask(projectID, taskID); result.Err != nil {
		return nil, result.Err
	} else {
		if result := <-Srv.Store.Task().Get(projectID, taskID); result.Err != nil {
			return nil, result.Err
		} else {
			task := result.Data.(model.Task)
			return &task, nil
		}
	}
}
