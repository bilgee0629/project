package app

import (
	"fmt"

	"bitbucket.org/bilgee0629/project/model"
)

func CreateProject(project *model.Project) (*model.Project, error) {
	if project == nil {
		return nil, fmt.Errorf("invalid payload")
	}
	if result := <-Srv.Store.Project().Save(project); result.Err != nil {
		return nil, result.Err
	} else {
		resultProject := result.Data.(*model.Project)

		return resultProject, nil
	}
}

func GetProjects() ([]model.Project, error) {
	if result := <-Srv.Store.Project().GetAll(); result.Err != nil {
		return nil, result.Err
	} else {
		resultProjects := result.Data.([]model.Project)
		return resultProjects, nil
	}
}

func GetFilteredProjects(name string, limit int, offset int) ([]model.Project, error) {
	if result := <-Srv.Store.Project().GetFilteredProjects(name, limit, offset); result.Err != nil {
		return nil, result.Err
	} else {
		resultProjects := result.Data.([]model.Project)
		return resultProjects, nil
	}
}
