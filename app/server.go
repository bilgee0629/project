package app

import (
	"net/http"
	"time"

	"bitbucket.org/bilgee0629/project/store"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/tylerb/graceful"
)

type Server struct {
	Store          store.Store
	Router         *mux.Router
	GracefulServer *graceful.Server
}

var Srv *Server

func NewServer() {
	Srv = &Server{}

	Srv.Store = store.NewMongoStore()
	Srv.Router = mux.NewRouter()

	Srv.GracefulServer = &graceful.Server{
		Server: &http.Server{
			Addr:    viper.GetString("server.address"),
			Handler: Srv.Router,
		},
	}
}

func StartServer() {
	logrus.Info("Starting server")

	go func() {
		err := Srv.GracefulServer.ListenAndServe()
		if err != nil {
			logrus.Error(err)
			time.Sleep(time.Second)
		}
	}()
}

func StopServer() {
	logrus.Info("Stopping server")
	Srv.GracefulServer.Stop(0)
}
