package main

import (
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/bilgee0629/project/api"
	"bitbucket.org/bilgee0629/project/app"
	"bitbucket.org/bilgee0629/project/utils"
)

func main() {

	utils.LoadConfig([]string{"./config"}, "config")

	app.NewServer()
	api.InitAPI()

	app.StartServer()

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	<-c

	app.StopServer()
}
