### To run the app
Get the project using ```go get bitbucket.org/bilgee0629/project```
Go to project folder using ```cd $GOPATH/src/bitbucket.org/bilgee0629/project```
You can simply run it by ```go run main.go``` from the project folder

### API endpoints
* POST /api/v1/projects (creates a new project)
* GET /api/v1/projects (retrieves all projects. Also other url parameters can be used such as name, limit, offset)
---
* POST /api/v1/projects/{id}/tasks (adds new tasks to given project)
* GET /api/v1/projects/{id}/tasks (returns all tasks for given project id. Also other url parameter can be used such as status)
* GET /api/v1/projects/{id}/tasks/{task_id}/close (changes single task status to closed)
